import pymysql
import json
import math
from . import dbinfo
from datetime import datetime, timedelta
from tqdm import tqdm

def save_places(items):
    inserted = False
    try:
        db_info = dbinfo.getDBInfo()
        connection = pymysql.connect(host = db_info['host'],
        port = db_info['port'], user = db_info['user'], password = db_info['password'],
        db = db_info['db'])

        curs = connection.cursor(pymysql.cursors.DictCursor)

        tag = items['tag']
        name = items['location']
        url = items['url']
        if 'latitude' in items:
            latitude = items['latitude']
        else:
            latitude = None
        if 'longitude' in items:
            latitude = items['longitude']
        else:
            longitude = None
        
        sql = "select count(*) cnt from places where url = '%s'" % (items['url'])
        curs.execute(sql)
        result = curs.fetchone()
        
        if result['cnt'] == 0:
            sql = "insert into places (tag, name, url, latitude, longitude, created_at) values (%s, %s, %s, %s, %s, now())"
            curs.execute(sql, (tag, name, url, latitude, longitude))
            connection.commit()
            inserted = True
        connection.close()
    except Exception as ex:
        print('error@save_places', ex)
        # print(items)
    finally:
        return inserted

def check_posts(url):
    # 이미 저장된 post는 False를 반환
    try:
        db_info = dbinfo.getDBInfo()
        connection = pymysql.connect(host = db_info['host'],
        port = db_info['port'], user = db_info['user'], password = db_info['password'],
        db = db_info['db'])
        curs = connection.cursor(pymysql.cursors.DictCursor)

        sql = "select count(*) cnt from posts where url = '%s'" % (url)
        curs.execute(sql)
        result = curs.fetchone()
        if result['cnt'] != 0:
            return False
        else:
            return True

    except Exception as ex:
        print('error@check_posts', ex)
        connection.rollback()
    finally:
        if connection.open:
            curs.close()
            connection.close()

def save_posts(places_id, items):
    # 종종 network문제로 정보가 누락되어 수집될 경우 DB저장 안함
    if (items['user_id'] == '' or items['user_name'] == '' or items['datetime'] == ''):
        return
    try:
        db_info = dbinfo.getDBInfo()
        connection = pymysql.connect(host = db_info['host'],
        port = db_info['port'], user = db_info['user'], password = db_info['password'],
        db = db_info['db'])
        curs = connection.cursor(pymysql.cursors.DictCursor)

        # 이미 수집된 post일 경우 pass~
        sql = "select count(*) cnt from posts where url = '%s'" % (items['key'])
        curs.execute(sql)
        result = curs.fetchone()
        if result['cnt'] != 0:
            return
        # user
        sql = "insert into users (user_id, user_name, created_at) values (%s, %s, now()) on duplicate key update id = LAST_INSERT_ID(id)"
        curs.execute(sql, (items['user_id'], items['user_name']))
        users_id = curs.lastrowid
        # post
        # post의 생성일을 한국기준시간으로 변환
        postdate = (datetime.fromisoformat(items['datetime'][:19]) + timedelta(hours=9)).isoformat(' ')
        sql = "insert into posts (users_id, places_id, url, post_datetime, created_at) values (%s, %s, %s, %s, now()) on duplicate key update id = LAST_INSERT_ID(id)"
        
        curs.execute(sql, (users_id, places_id, items['key'], postdate))
        posts_id = curs.lastrowid
        # img
        if 'img_urls' in items:
            for img in items['img_urls']:
                sql = "insert into posts_imgs (posts_id, url, created_at) values (%s, %s, now())"
                curs.execute(sql, (posts_id, img))
        # desc의 tag
        if 'hashtags' in items:
            for tag in items['hashtags']:
                sql = "insert into tags (tag, created_at) values (%s, now()) on duplicate key update id = LAST_INSERT_ID(id)"
                curs.execute(sql, (tag))
                tags_id = curs.lastrowid
                sql = "insert into posts_tags (posts_id, tags_id) values (%s, %s) on duplicate key update tags_id = tags_id"
                curs.execute(sql, (posts_id, tags_id))
        #comments의 tag
        if 'comments' in items:
            for comment in items['comments']:
                if 'hashtags' in comment:
                    for tag in comment['hashtags']:
                        sql = "insert into tags (tag, created_at) values (%s, now()) on duplicate key update id = LAST_INSERT_ID(id)"
                        curs.execute(sql, (tag))
                        tags_id = curs.lastrowid
                        sql = "insert into posts_tags (posts_id, tags_id) values (%s, %s) on duplicate key update tags_id = tags_id"
                        curs.execute(sql, (posts_id, tags_id))

        connection.commit()
    
    except Exception as ex:
        print('error@save_posts', ex)
        connection.rollback()
    finally:
        if connection.open:
            curs.close()
            connection.close()

def verify_distance(tag, tag_coord, distance):
    db_info = dbinfo.getDBInfo()
    connection = pymysql.connect(host = db_info['host'],
    port = db_info['port'], user = db_info['user'], password = db_info['password'],
    db = db_info['db'])

    curs = connection.cursor(pymysql.cursors.DictCursor)
    # sql = "select * from places where tag = '%s'" % (tag)
    sql = "select * from places where latitude is not null and longitude is not null"
    curs.execute(sql)
    rows = curs.fetchall()
    pbar = tqdm(rows)
    for row in pbar:
        pbar.set_description('verifing')
        if _get_distance(tag_coord, (float(row['latitude']), float(row['longitude']))) < distance:
            # sql = "update places set distance_verified = 'Y' where id = %d" % (row['id'])
            sql = "update places set should_crawl = 'Y' where id = %d" % (row['id'])
            curs.execute(sql)
            connection.commit()
        else:
            # sql = "update places set distance_verified = 'N' where id = %d" % (row['id'])
            sql = "update places set should_crawl = 'N' where id = %d" % (row['id'])
            curs.execute(sql)
            connection.commit()
    connection.close()

def _get_distance(coord1, coord2):
    # print(coord1)
    # print(coord2)
    R = 6372800  # Earth radius in meters
    lat1, lon1 = coord1
    lat2, lon2 = coord2
    
    phi1, phi2 = math.radians(lat1), math.radians(lat2) 
    dphi       = math.radians(lat2 - lat1)
    dlambda    = math.radians(lon2 - lon1)
    
    a = math.sin(dphi/2)**2 + \
        math.cos(phi1)*math.cos(phi2)*math.sin(dlambda/2)**2
    
    return int(2*R*math.atan2(math.sqrt(a), math.sqrt(1 - a)) / 1000)    

def get_places_by_tag(tag):
    db_info = dbinfo.getDBInfo()
    connection = pymysql.connect(host = db_info['host'],
    port = db_info['port'], user = db_info['user'], password = db_info['password'],
    db = db_info['db'])

    curs = connection.cursor(pymysql.cursors.DictCursor)
    # print("tag : %s" % tag)
    sql = "select * from places where tag like %s and geocode_checked is null" 
    curs.execute(sql, tag + "%")
    rows = curs.fetchall()
    connection.close()
    return rows

def get_places_for_crawl():
    db_info = dbinfo.getDBInfo()
    connection = pymysql.connect(host = db_info['host'],
    port = db_info['port'], user = db_info['user'], password = db_info['password'],
    db = db_info['db'])

    curs = connection.cursor(pymysql.cursors.DictCursor)
    sql = "select id, name, url from places where should_crawl = 'Y' and (is_banned != 'Y' or is_banned is null)" 
    curs.execute(sql)
    rows = curs.fetchall()
    connection.close()
    return rows

def update_geocode(dict_geocode):
    db_info = dbinfo.getDBInfo()
    connection = pymysql.connect(host = db_info['host'],
    port = db_info['port'], user = db_info['user'], password = db_info['password'],
    db = db_info['db'])

    curs = connection.cursor(pymysql.cursors.DictCursor)
    
    sql = "update places set latitude = %s, longitude = %s, geocode_checked = %s where id = %s"
    # print(sql)
    curs.execute(sql, (dict_geocode['latitude'], dict_geocode['longitude'], 'Y', dict_geocode['id']))
    connection.commit()
    connection.close()

def get_posts_count(id):
    db_info = dbinfo.getDBInfo()
    connection = pymysql.connect(host = db_info['host'],
    port = db_info['port'], user = db_info['user'], password = db_info['password'],
    db = db_info['db'])

    curs = connection.cursor(pymysql.cursors.DictCursor)
    sql = "select count(id) cnt from posts where places_id = %s" 
    curs.execute(sql, (id))
    result = curs.fetchone()
    connection.close()
    return result['cnt']
    
def get_users():
    db_info = dbinfo.getDBInfo()
    connection = pymysql.connect(host = db_info['host'],
    port = db_info['port'], user = db_info['user'], password = db_info['password'],
    db = db_info['db'])

    curs = connection.cursor(pymysql.cursors.DictCursor)
    sql = "select id, user_name from users" 
    curs.execute(sql)
    rows = curs.fetchall()
    connection.close()
    return rows

def update_users(dict_user):
    db_info = dbinfo.getDBInfo()
    connection = pymysql.connect(host = db_info['host'],
    port = db_info['port'], user = db_info['user'], password = db_info['password'],
    db = db_info['db'])

    curs = connection.cursor(pymysql.cursors.DictCursor)
    
    sql = "update users set user_name = %s, posts_count = %s, follow_count = %s, followed_count = %s, updated_at = now() where id = %s"
    # print(sql)
    curs.execute(sql, (dict_user['user_name'], dict_user['posts'], dict_user['follow'], dict_user['followed'], dict_user['id']))
    connection.commit()
    connection.close()  

def get_post_by_user(id):
    db_info = dbinfo.getDBInfo()
    connection = pymysql.connect(host = db_info['host'],
    port = db_info['port'], user = db_info['user'], password = db_info['password'],
    db = db_info['db'])

    curs = connection.cursor(pymysql.cursors.DictCursor)
    
    sql = "select url from posts where users_id = %s"
    curs.execute(sql, (id))
    result = curs.fetchone()
    connection.close()

    return result['url']  