from __future__ import unicode_literals

import glob
import json
import os
import re
import sys
import time
import traceback
from builtins import open
from time import sleep

from tqdm import tqdm

from . import secret
from .browser import Browser
from .exceptions import RetryException
from .fetch import fetch_caption
from .fetch import fetch_comments
from .fetch import fetch_datetime
from .fetch import fetch_imgs
from .fetch import fetch_likers
from .fetch import fetch_likes_plays
from .fetch import fetch_details
from .fetch import fetch_places
from .fetch import fetch_geocodes
from .fetch import fetch_users_info

from .utils import instagram_int
from .utils import randmized_sleep
from .utils import retry

from .db import save_places
from .db import save_posts
from .db import get_places_by_tag
from .db import update_geocode
from .db import get_places_for_crawl
from .db import check_posts
from .db import get_posts_count
from .db import get_users
from .db import update_users

from .settings import settings

from datetime import datetime

from torpy.http.requests import TorRequests

class Logging(object):
    PREFIX = "instagram-crawler"

    def __init__(self):
        try:
            timestamp = int(time.time())
            self.cleanup(timestamp)
            self.logger = open("/tmp/%s-%s.log" % (Logging.PREFIX, timestamp), "w")
            self.log_disable = False
        except Exception:
            self.log_disable = True

    def cleanup(self, timestamp):
        days = 86400 * 7
        days_ago_log = "/tmp/%s-%s.log" % (Logging.PREFIX, timestamp - days)
        for log in glob.glob("/tmp/instagram-crawler-*.log"):
            if log < days_ago_log:
                os.remove(log)

    def log(self, msg):
        if self.log_disable:
            return

        self.logger.write(msg + "\n")
        self.logger.flush()

    def __del__(self):
        if self.log_disable:
            return
        self.logger.close()


class InsCrawler(Logging):
    URL = "https://www.instagram.com"
    RETRY_LIMIT = 10

    def __init__(self, has_screen=False):
        super(InsCrawler, self).__init__()
        self.browser = Browser(has_screen)
        self.page_height = 0
        #self.login()

    def _dismiss_login_prompt(self):
        ele_login = self.browser.find_one(".Ls00D .Szr5J")
        if ele_login:
            ele_login.click()

    def login(self):
        browser = self.browser
        url = "%s/accounts/login/" % (InsCrawler.URL)
        browser.get(url)
        u_input = browser.find_one('input[name="username"]')
        u_input.send_keys(secret.username)
        p_input = browser.find_one('input[name="password"]')
        p_input.send_keys(secret.password)

        login_btn = browser.find_one(".L3NKy")
        login_btn.click()

        @retry()
        def check_login():
            if browser.find_one('input[name="username"]'):
                raise RetryException()

        check_login()
    
    def login_multi(self, index):
        browser = self.browser
        url = "%s/accounts/login/" % (InsCrawler.URL)
        browser.get(url)
        u_input = browser.find_one('input[name="username"]')
        u_input.send_keys(secret.usernames[index % len(secret.usernames)])
        p_input = browser.find_one('input[name="password"]')
        p_input.send_keys('Tldptmxk2018')

        login_btn = browser.find_one(".L3NKy")
        login_btn.click()

        @retry()
        def check_login():
            if browser.find_one('input[name="username"]'):
                raise RetryException()

        check_login()

    def get_user_profile(self, username):
        browser = self.browser
        url = "%s/%s/" % (InsCrawler.URL, username)
        browser.get(url)
        name = browser.find_one(".rhpdm")
        desc = browser.find_one(".-vDIg span")
        photo = browser.find_one("._6q-tv")
        statistics = [ele.text for ele in browser.find(".g47SY")]
        post_num, follower_num, following_num = statistics
        return {
            "name": name.text,
            "desc": desc.text if desc else None,
            "photo_url": photo.get_attribute("src"),
            "post_num": post_num,
            "follower_num": follower_num,
            "following_num": following_num,
        }

    def get_user_profile_from_script_shared_data(self, username):
        browser = self.browser
        url = "%s/%s/" % (InsCrawler.URL, username)
        browser.get(url)
        source = browser.driver.page_source
        p = re.compile(r"window._sharedData = (?P<json>.*?);</script>", re.DOTALL)
        json_data = re.search(p, source).group("json")
        data = json.loads(json_data)

        user_data = data["entry_data"]["ProfilePage"][0]["graphql"]["user"]

        return {
            "name": user_data["full_name"],
            "desc": user_data["biography"],
            "photo_url": user_data["profile_pic_url_hd"],
            "post_num": user_data["edge_owner_to_timeline_media"]["count"],
            "follower_num": user_data["edge_followed_by"]["count"],
            "following_num": user_data["edge_follow"]["count"],
            "website": user_data["external_url"],
        }
    def get_user_profile_from_post(self, dict_post):
        #로그인 안한 상태에서 포스팅 한 유저정보 가져오기
        browser = self.browser
        scripts = browser.find("script")
        source = ""
        for script in scripts:
            # print("text : %s" % (script.get_attribute('innerText')))
            if script.get_attribute('innerText').find("window._sharedData = {") != -1:
                # print("found!")
                source = script.get_attribute('outerHTML')
                break
        
        
        p = re.compile(r"window._sharedData = (?P<json>.*?);</script>", re.DOTALL)
        json_data = re.search(p, source).group("json")
        data = json.loads(json_data)
        user_data = data["entry_data"]["PostPage"][0]["graphql"]["shortcode_media"]["owner"]
        dict_post["user_id"] = user_data["id"]
        dict_post["user_name"] = user_data["username"]
        # print("usrname : %s" % (user_data["username"]))
    
    def get_user_profile_from_post_with_login(self, dict_post):
        #로그인 한 상태에서 포스팅 한 유저정보 가져오기
        browser = self.browser
        scripts = browser.find("script")
        source = ""
        for script in scripts:
            # print("text : %s" % (script.get_attribute('innerText')))
            if script.get_attribute('innerText').find("window.__additionalDataLoaded(") != -1:
                # print("found!")
                source = script.get_attribute('outerHTML')
                break
        # source = browser.driver.page_source
        
        postkey = dict_post['key'].replace(InsCrawler.URL, '')
        p = re.compile(r"window.__additionalDataLoaded\('" + postkey + r"',(?P<json>.*?)\);</script>", re.DOTALL)
        if re.search(p, source) is not None:
            json_data = re.search(p, source).group("json")
            data = json.loads(json_data)
            user_data = data["graphql"]["shortcode_media"]["owner"]
            dict_post["user_id"] = user_data["id"]
            dict_post["user_name"] = user_data["username"]
            # print("usrname : %s" % (user_data["username"]))
        else:
            for script in scripts:
            # print("text : %s" % (script.get_attribute('innerText')))
                if script.get_attribute('innerText').find("window._sharedData = {") != -1:
                    # print("found!")
                    source = script.get_attribute('outerHTML')
                    break
            p = re.compile(r"window._sharedData = (?P<json>.*?);</script>", re.DOTALL)

            if re.search(p, source) is not None:
                json_data = re.search(p, source).group("json")
                data = json.loads(json_data)
                user_data = data["entry_data"]["PostPage"][0]["graphql"]["shortcode_media"]["owner"]
                dict_post["user_id"] = user_data["id"]
                dict_post["user_name"] = user_data["username"]
            else:
                print("couldn't get user information!")
                print(dict_post['key'])
                dict_post["user_id"] = ""
                dict_post["user_name"] = ""
            
    def get_user_posts(self, username, number=None, detail=False):

        user_profile = self.get_user_profile(username)
        if not number:
            number = instagram_int(user_profile["post_num"])

        self._dismiss_login_prompt()

        if detail:
            return self._get_posts_full(number)
        else:
            return self._get_posts(number)

    def get_latest_posts_by_tag(self, tag, num):
        url = "%s/explore/tags/%s/" % (InsCrawler.URL, tag)
        self.browser.get(url)
        return self._get_posts(num)

    def get_places_by_tag(self, tag, num):
        #self.login()
        
        url = "%s/explore/tags/%s/" % (InsCrawler.URL, tag)
        self.browser.get(url)
        return self._get_places(tag, num)

    def get_location_postno(self, location_url):
        browser = self.browser
        browser.get(location_url)
        source = browser.driver.page_source
        p = re.compile(r"window._sharedData = (?P<json>.*?);</script>", re.DOTALL)
        json_data = re.search(p, source).group("json")
        data = json.loads(json_data)

        location_data = data["entry_data"]["LocationsPage"][0]["graphql"]["location"]

        return location_data["edge_location_to_media"]["count"]

    def get_location_posts(self):
        self.login()
        posts = []
        rows = get_places_for_crawl()
        pbar = tqdm(rows)
        
        # posts_count = self.get_location_postno("https://www.instagram.com/explore/locations/107463737541977/")
        # return self._get_posts_by_location(10)

        for row in pbar:
            pbar.set_description(row['name'])
            posts_count = self.get_location_postno(row['url'])
            posts.append(self._get_posts_by_location(row['id'], posts_count, "", 0, 0)) 
            # posts.append(self._get_posts_by_location(row['id'], 10)) 
        
        return posts
    
    def get_location_posts_by_id(self, index, total):
        self.login_multi(index)
        posts = []
        rows = get_places_for_crawl()
        pbar = tqdm(rows)
        
        id = 0

        for row in pbar:
            if (id % total) == index:
                # print(id)
                pbar.set_description(row['name'])
                posts_count = self.get_location_postno(row['url'])
                posts.append(self._get_posts_by_location(row['id'], posts_count, row['name'], index, total)) 
            id = id + 1
        return posts

    def auto_like(self, tag="", maximum=1000):
        self.login()
        browser = self.browser
        if tag:
            url = "%s/explore/tags/%s/" % (InsCrawler.URL, tag)
        else:
            url = "%s/explore/" % (InsCrawler.URL)
        self.browser.get(url)

        ele_post = browser.find_one(".v1Nh3 a")
        ele_post.click()

        for _ in range(maximum):
            heart = browser.find_one(".dCJp8 .glyphsSpriteHeart__outline__24__grey_9")
            if heart:
                heart.click()
                randmized_sleep(2)

            left_arrow = browser.find_one(".HBoOv")
            if left_arrow:
                left_arrow.click()
                randmized_sleep(2)
            else:
                break

    def _get_posts_full(self, num):
        @retry()
        def check_next_post(cur_key):
            ele_a_datetime = browser.find_one(".eo2As .c-Yi7")

            # It takes time to load the post for some users with slow network
            if ele_a_datetime is None:
                raise RetryException()

            next_key = ele_a_datetime.get_attribute("href")
            if cur_key == next_key:
                raise RetryException()

        browser = self.browser
        browser.implicitly_wait(1)
        browser.scroll_down()
        ele_post = browser.find_one(".v1Nh3 a")
        ele_post.click()
        dict_posts = {}

        pbar = tqdm(total=num)
        pbar.set_description("fetching")
        cur_key = None

        all_posts = self._get_posts(num)
        i = 1

        # Fetching all posts
        for _ in range(num):
            dict_post = {}

            # Fetching post detail
            try:
                if(i < num):
                    check_next_post(all_posts[i]['key'])
                    i = i + 1

                # Fetching datetime and url as key
                ele_a_datetime = browser.find_one(".eo2As .c-Yi7")
                cur_key = ele_a_datetime.get_attribute("href")
                dict_post["key"] = cur_key
                fetch_datetime(browser, dict_post)
                fetch_imgs(browser, dict_post)
                fetch_likes_plays(browser, dict_post)
                fetch_likers(browser, dict_post)
                fetch_caption(browser, dict_post)
                fetch_comments(browser, dict_post)

            except RetryException:
                sys.stderr.write(
                    "\x1b[1;31m"
                    + "Failed to fetch the post: "
                    + cur_key or 'URL not fetched'
                    + "\x1b[0m"
                    + "\n"
                )
                break

            except Exception:
                sys.stderr.write(
                    "\x1b[1;31m"
                    + "Failed to fetch the post: "
                    + cur_key if isinstance(cur_key,str) else 'URL not fetched'
                    + "\x1b[0m"
                    + "\n"
                )
                traceback.print_exc()

            self.log(json.dumps(dict_post, ensure_ascii=False))
            dict_posts[browser.current_url] = dict_post

            pbar.update(1)

        pbar.close()
        posts = list(dict_posts.values())
        if posts:
            posts.sort(key=lambda post: post["datetime"], reverse=True)
        return posts

    def _get_posts(self, num):
        """
            To get posts, we have to click on the load more
            button and make the browser call post api.
        """
        TIMEOUT = 600
        browser = self.browser
        key_set = set()
        posts = []
        pre_post_num = 0
        wait_time = 1

        pbar = tqdm(total=num)

        def start_fetching(pre_post_num, wait_time):
            ele_posts = browser.find(".v1Nh3 a")
            for ele in ele_posts:
                key = ele.get_attribute("href")
                if key not in key_set:
                    dict_post = { "key": key }
                    ele_img = browser.find_one(".KL4Bh img", ele)
                    dict_post["caption"] = ele_img.get_attribute("alt")
                    dict_post["img_url"] = ele_img.get_attribute("src")

                    fetch_details(browser, dict_post)

                    key_set.add(key)
                    posts.append(dict_post)

                    if len(posts) == num:
                        break

            if pre_post_num == len(posts):
                pbar.set_description("Wait for %s sec" % (wait_time))
                sleep(wait_time)
                pbar.set_description("fetching")

                wait_time *= 2
                browser.scroll_up(300)
            else:
                wait_time = 1

            pre_post_num = len(posts)
            browser.scroll_down()

            return pre_post_num, wait_time

        pbar.set_description("fetching")
        while len(posts) < num and wait_time < TIMEOUT:
            post_num, wait_time = start_fetching(pre_post_num, wait_time)
            pbar.update(post_num - pre_post_num)
            pre_post_num = post_num

            loading = browser.find_one(".W1Bne")
            if not loading and wait_time > TIMEOUT / 2:
                break

        pbar.close()
        print("Done. Fetched %s posts." % (min(len(posts), num)))
        return posts[:num]

    def _get_places(self, tag, num):
        """
            To get posts, we have to click on the load more
            button and make the browser call post api.
        """
        TIMEOUT = 600
        browser = self.browser
        key_set = set()
        posts = []
        pre_post_num = 0
        wait_time = 1
        pbar = tqdm(total=num)

        def start_fetching(pre_post_num, wait_time):
            ele_posts = browser.find(".v1Nh3 a")
            for ele in ele_posts:
                key = ele.get_attribute("href")
                if key not in key_set:
                    dict_post = { "key": key }
                    ele_img = browser.find_one(".KL4Bh img", ele)
                    # dict_post["caption"] = ele_img.get_attribute("alt")
                    # dict_post["img_url"] = ele_img.get_attribute("src")
                    if ele_img.get_attribute("alt").find(" in ") != -1:
                        dict_post["tag"] = tag
                        fetch_places(browser, dict_post)
                        save_places(dict_post)
                        posts.append(dict_post)
                    
                    key_set.add(key)
                    
                    if len(posts) == num:
                        break
            pbar.set_description("fetching")
            if pre_post_num == len(posts):
                pbar.set_description("Wait for %s sec" % (wait_time))
                sleep(wait_time)
                pbar.set_description("fetching")

                wait_time *= 2
                browser.scroll_up(300)
            else:
                wait_time = 1

            pre_post_num = len(posts)
            browser.scroll_down()

            return pre_post_num, wait_time
        
        pbar.set_description("fetching")
        while len(posts) < num and wait_time < TIMEOUT:
            post_num, wait_time = start_fetching(pre_post_num, wait_time)
            pbar.update(post_num - pre_post_num)
            pre_post_num = post_num

            loading = browser.find_one(".W1Bne")
            if not loading and wait_time > TIMEOUT / 2:
                break

        pbar.close()
        print("Done. Fetched %s posts." % (min(len(posts), num)))
        return posts[:num]

    # def update_geocode(self, tag):
    #     rows = get_places_by_tag(tag)
    #     pbar = tqdm(rows)
    #     index = 0
    #     for row in pbar:
    #         index = index + 1
    #         # print("tag: %s name: %s" % (row['tag'],row['name']))
    #         dict_geocode = {"id": row['id'], "url": row['url']}
    #         fetch_geocodes(self.browser, dict_geocode, index)
    #         update_geocode(dict_geocode)
    #     print("count:%i" % len(rows))

    def update_geocode(self, tag):
        rows = get_places_by_tag(tag)
        pbar = tqdm(rows)
        index = 0
        with TorRequests() as tor_requests:
            with tor_requests.get_session() as session:
                for row in pbar:
                    index = index + 1
                    dict_geocode = {"id": row['id'], "url": row['url']}
                    if index % 20 == 0:
                        ctx = tor_requests.get_session()
                        session = ctx.__enter__() # pylint: disable=no-member
                    fetch_geocodes(self.browser, dict_geocode, session)
                    # print(dict_geocode["latitude"])
                    # print(dict_geocode["longitude"])
                    update_geocode(dict_geocode)
        
        print("count:%i" % len(rows))

    def get_users_info(self, index, total):
        self.login_multi(index)
        rows = get_users()
        pbar = tqdm(rows)

        id = 0

        for row in pbar:
            if (id % total) == index:
                pbar.set_description(row['user_name'])
                dict_user = {"id": row['id'], "user_name": row['user_name']}
                fetch_users_info(self.browser, dict_user)
                if dict_user["user_name"] != '':
                    update_users(dict_user)
                sleep(18)
            id = id + 1
        print("count:%i" % len(rows))

    def _get_posts_by_location(self, id, num, name, index, total):
        TIMEOUT = 100
        browser = self.browser
        key_set = set()
        posts = []
        pre_post_num = 0
        wait_time = 1
        pbar = tqdm(total=num)

        settings.fetch_hashtags = True
        settings.fetch_comments = True

        duplicated_posts = 0
        posts_count = get_posts_count(id)
        self.output("%s: %s[%s/%s] start" % (index, name, posts_count, num))
        def start_fetching(pre_post_num, wait_time, cnt):
            ele_posts = browser.find(".v1Nh3 a")
            for ele in ele_posts:
                key = ele.get_attribute("href")
                if key not in key_set:
                    dict_post = { "key": key }
                    if check_posts(key):
                        cnt = 0
                        # ele_img = browser.find_one(".KL4Bh img", ele)
                        # dict_post["caption"] = ele_img.get_attribute("alt")
                        # dict_post["img_url"] = ele_img.get_attribute("src")
                        browser.open_new_tab(dict_post["key"])
                        # self.get_user_profile_from_post(dict_post)
                        self.get_user_profile_from_post_with_login(dict_post)
                        fetch_datetime(browser, dict_post)
                        fetch_imgs(browser, dict_post)
                        # fetch_likes_plays(browser, dict_post)
                        # fetch_likers(browser, dict_post)
                        fetch_caption(browser, dict_post)
                        fetch_comments(browser, dict_post)
                        browser.close_current_tab()
                        save_posts(id, dict_post)
                    else:
                        cnt = cnt + 1    
                    posts.append(dict_post)    
                    key_set.add(key)
                    
                    if len(posts) == num:
                        break
            pbar.set_description("fetching")
            if pre_post_num == len(posts):
                pbar.set_description("Wait for %s sec" % (wait_time))
                sleep(wait_time)
                pbar.set_description("fetching")

                wait_time *= 2
                browser.scroll_up(300)
            else:
                wait_time = 1

            pre_post_num = len(posts)
            browser.scroll_down()

            return pre_post_num, wait_time, cnt
        
        pbar.set_description("fetching")
        while len(posts) < num and wait_time < TIMEOUT:
            post_num, wait_time, duplicated_posts = start_fetching(pre_post_num, wait_time, duplicated_posts)
            # print("saved posts : %s" % duplicated_posts)
            if posts_count > (num * 0.9) and duplicated_posts > 100:
                self.output("%s: %s[%s/%s] %s fetched and skipped because of duplicated posts." % (index, name, posts_count, num, len(posts)))
                print("cralwing is skipped because of duplicated posts")
                pbar.close()
                return posts[:num]
            pbar.update(post_num - pre_post_num)
            pre_post_num = post_num

            loading = browser.find_one(".W1Bne")
            if not loading and wait_time > TIMEOUT / 2:
                break

        pbar.close()
        self.output("%s: %s[%s/%s] %s fetched" % (index, name, posts_count, num, len(posts)))
        print("Done. Fetched %s posts." % (min(len(posts), num)))
        return posts[:num]

    def output(self, data):
        now = datetime.now()
        with open("log.txt", "a", encoding="utf8") as f:
            f.write("%s %s\n" % (now.strftime("%Y-%m-%d, %H:%M:%S"), data))