import re
from time import sleep
from .settings import settings
from bs4 import BeautifulSoup as bs
import requests
from selenium.common.exceptions import StaleElementReferenceException
# from selenium.webdriver.common.keys import Keys
import json

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from .db import get_post_by_user

def get_parsed_mentions(raw_text):
    regex = re.compile(r"@([\w\.]+)")
    regex.findall(raw_text)
    return regex.findall(raw_text)


def get_parsed_hashtags(raw_text):
    regex = re.compile(r"#(\w+)")
    regex.findall(raw_text)
    return regex.findall(raw_text)


def fetch_mentions(raw_test, dict_obj):
    if not settings.fetch_mentions: # pylint: disable=no-member
        return

    mentions = get_parsed_mentions(raw_test)
    if mentions:
        dict_obj["mentions"] = mentions

def fetch_hashtags(raw_test, dict_obj):
    if not settings.fetch_hashtags: # pylint: disable=no-member
        return

    hashtags = get_parsed_hashtags(raw_test)
    if hashtags:
        dict_obj["hashtags"] = hashtags


def fetch_datetime(browser, dict_post):
    ele_datetime = browser.find_one(".eo2As .c-Yi7 ._1o9PC")
    if ele_datetime is not None: 
        datetime = ele_datetime.get_attribute("datetime")
    else:
        datetime = ''
    dict_post["datetime"] = datetime

def fetch_imgs(browser, dict_post):
    try:
        img_urls = set()
        while True:
            ele_imgs = browser.find("._97aPb img", waittime=20)

            if isinstance(ele_imgs, list):
                for ele_img in ele_imgs:
                    img_urls.add(ele_img.get_attribute("src"))
            else:
                break

            next_photo_btn = browser.find_one("._6CZji .coreSpriteRightChevron")

            if next_photo_btn:
                next_photo_btn.click()
                sleep(0.3)
            else:
                break

        dict_post["img_urls"] = list(img_urls)
    except StaleElementReferenceException as e:
        print(e)
        pass
    

def fetch_likes_plays(browser, dict_post):
    if not settings.fetch_likes_plays: # pylint: disable=no-member
        return

    likes = None
    el_likes = browser.find_one(".Nm9Fw > * > span")
    el_see_likes = browser.find_one(".vcOH2")

    if el_see_likes is not None:
        el_plays = browser.find_one(".vcOH2 > span")
        dict_post["views"] = int(el_plays.text.replace(",", "").replace(".", ""))
        el_see_likes.click()
        el_likes = browser.find_one(".vJRqr > span")
        likes = el_likes.text
        browser.find_one(".QhbhU").click()

    elif el_likes is not None:
        likes = el_likes.text

    dict_post["likes"] = (
        int(likes.replace(",", "").replace(".", "")) if likes is not None else 0
    )


def fetch_likers(browser, dict_post):
    if not settings.fetch_likers: # pylint: disable=no-member
        return
    like_info_btn = browser.find_one(".EDfFK ._0mzm-.sqdOP")
    like_info_btn.click()

    likers = {}
    liker_elems_css_selector = ".Igw0E ._7UhW9.xLCgt a"
    likers_elems = list(browser.find(liker_elems_css_selector))
    last_liker = None
    while likers_elems:
        for ele in likers_elems:
            likers[ele.get_attribute("href")] = ele.get_attribute("title")

        if last_liker == likers_elems[-1]:
            break

        last_liker = likers_elems[-1]
        last_liker.location_once_scrolled_into_view
        sleep(0.6)
        likers_elems = list(browser.find(liker_elems_css_selector))

    dict_post["likers"] = list(likers.values())
    close_btn = browser.find_one(".WaOAr button")
    close_btn.click()


def fetch_caption(browser, dict_post):
    ele_comments = browser.find(".eo2As .gElp9")

    if len(ele_comments) > 0:

        temp_element = browser.find("span",ele_comments[0])

        for element in temp_element:

            if element.text not in ['Verified',''] and 'caption' not in dict_post:
                dict_post["caption"] = element.text

        fetch_mentions(dict_post.get("caption",""), dict_post)
        fetch_hashtags(dict_post.get("caption",""), dict_post)


def fetch_comments(browser, dict_post):
    if not settings.fetch_comments: # pylint: disable=no-member
        return

    show_more_selector = "button .glyphsSpriteCircle_add__outline__24__grey_9"
    show_more = browser.find_one(show_more_selector)
    while show_more:
        show_more.location_once_scrolled_into_view
        # show_more.click()
        browser.js_click(show_more)
        sleep(0.3)
        show_more = browser.find_one(show_more_selector)

    show_comment_btns = browser.find(".EizgU")
    for show_comment_btn in show_comment_btns:
        show_comment_btn.location_once_scrolled_into_view
        # show_comment_btn.click()
        browser.js_click(show_comment_btn)
        sleep(0.3)

    ele_comments = browser.find(".eo2As .gElp9")
    comments = []
    for els_comment in ele_comments[1:]:
        # author = browser.find_one(".FPmhX", els_comment).text
        author = browser.find_one(".sqdOP", els_comment).text

        temp_element = browser.find("span", els_comment)

        for element in temp_element:

            if element.text not in ['Verified','']:
                comment = element.text
            else:
                comment = ''
        comment_obj = {"author": author, "comment": comment}

        fetch_mentions(comment, comment_obj)
        fetch_hashtags(comment, comment_obj)

        comments.append(comment_obj)

    if comments:
        dict_post["comments"] = comments


def fetch_initial_comment(browser, dict_post):
    comments_elem = browser.find_one("ul.XQXOT")
    first_post_elem = browser.find_one(".ZyFrc", comments_elem)
    caption = browser.find_one("span", first_post_elem)

    if caption:
        dict_post["description"] = caption.text


def fetch_details(browser, dict_post):
    if not settings.fetch_details: # pylint: disable=no-member
        return

    browser.open_new_tab(dict_post["key"])

    username = browser.find_one("a.ZIAjV")
    location = browser.find_one("a.O4GlU")

    if username:
        dict_post["username"] = username.text
    if location:
        dict_post["location"] = location.text
            
    fetch_initial_comment(browser, dict_post)

    browser.close_current_tab()

def fetch_places(browser, dict_post):
    try:
        browser.open_new_tab(dict_post["key"])
        location = browser.find_one("a.O4GlU")

        if location:
            dict_post["location"] = location.text
            dict_post["url"] = location.get_attribute("href")

            # response = browser.page_source(location.get_attribute("href"), location.text)
            # html = bs(response, 'html.parser')

            # # response = requests.get(location.get_attribute("href"))
            # # html = bs(response.text, 'html.parser')

            # dict_post["latitude"] = html.find('meta', {'property':'place:location:latitude'}).get('content')
            # dict_post["longitude"] = html.find('meta', {'property':'place:location:longitude'}).get('content')
    except Exception as ex:
        print('error@fetch_places', ex)
        print(dict_post)
    finally:
        browser.close_current_tab()
    
# def fetch_geocodes(browser, dict_geocode, index):
    
#     response = browser.page_source(dict_geocode["url"], "")
#     html = bs(response, 'html.parser')
    
#     # response = requests.get(dict_geocode["url"])
#     # html = bs(response.text, 'html.parser')

#     # dict_geocode["latitude"] = html.find('meta', {'property':'place:location:latitude'}).get('content')
#     # dict_geocode["longitude"] = html.find('meta', {'property':'place:location:longitude'}).get('content')

#     latitude = html.find('meta', {'property':'place:location:latitude'})
#     longitude = html.find('meta', {'property':'place:location:longitude'})
#     dict_geocode["latitude"] = None if latitude is None else latitude.get('content')
#     dict_geocode["longitude"] = None if longitude is None else longitude.get('content')

def fetch_geocodes(browser, dict_geocode, session):
    # print(session.get("http://httpbin.org/ip").json())
    response = session.get(dict_geocode["url"])
    html = bs(response.text, 'html.parser')
    # print(html)
    # dict_geocode["latitude"] = html.find('meta', {'property':'place:location:latitude'}).get('content')
    # dict_geocode["longitude"] = html.find('meta', {'property':'place:location:longitude'}).get('content')

    latitude = html.find('meta', {'property':'place:location:latitude'})
    longitude = html.find('meta', {'property':'place:location:longitude'})
    dict_geocode["latitude"] = None if latitude is None else latitude.get('content')
    dict_geocode["longitude"] = None if longitude is None else longitude.get('content')

def fetch_username(browser, url):
    browser.open_new_tab(url)
    userele = browser.find_one("a.ZIAjV")
    username = None
    if userele:
        username = userele.text
    browser.close_current_tab()
    return username
    
    # response = session.get(url)
    # source = response.text
    # p = re.compile(r"window._sharedData = (?P<json>.*?);</script>", re.DOTALL)
    # json_data = re.search(p, source).group("json")
    # data = json.loads(json_data)
    
    # return data["entry_data"]["PostPage"][0]["graphql"]["shortcode_media"]["owner"]["username"]

def fetch_users_info(browser, dict_user):
    try:
        url = "%s/%s/" % ("https://www.instagram.com", dict_user["user_name"])
        browser.get(url)
        
        source = browser.driver.page_source
        p = re.compile(r"window._sharedData = (?P<json>.*?);</script>", re.DOTALL)
        if re.search(p, source) is None:
            print("Error : shared data is not found.")
            username = fetch_username(browser, get_post_by_user(dict_user["id"]))
            # username이 None인 경우는 회원탈퇴로 우선 가정
            if username is not None:
                dict_user["user_name"] = username
                fetch_users_info(browser, dict_user)
            else:
                dict_user["user_name"] = ''
        else:
            json_data = re.search(p, source).group("json")
            data = json.loads(json_data)
            if "HttpErrorPage" in data["entry_data"]:
                print("Error : profile page is not found.")
                username = fetch_username(browser, get_post_by_user(dict_user["id"]))
                # username이 None인 경우는 회원탈퇴로 우선 가정
                if username is not None:
                    dict_user["user_name"] = username
                    fetch_users_info(browser, dict_user)
                else:
                    dict_user["user_name"] = ''
            else:
                user_data = data["entry_data"]["ProfilePage"][0]["graphql"]["user"]
                dict_user["followed"] = user_data["edge_followed_by"]["count"]
                dict_user["follow"] = user_data["edge_follow"]["count"]
                dict_user["posts"] = user_data["edge_owner_to_timeline_media"]["count"]
        
        
        
        # print(session.get("http://httpbin.org/ip").json())
        
        # url = "%s/%s/" % ("https://www.instagram.com", dict_user["user_name"])

        # session.headers = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36'}
        # session.headers.update({'Referer': 'https://www.naver.com/'})

        # response = session.get(url)
        # source = response.text
        # print(source)
        # p = re.compile(r"window._sharedData = (?P<json>.*?);</script>", re.DOTALL)
        # json_data = re.search(p, source).group("json")
        # data = json.loads(json_data)
        # #print(data)
        # if "HttpErrorPage" in data["entry_data"]:
        #     print("Error!")
        #     username = fetch_username(browser, get_post_by_user(dict_user["id"]), session)
        #     print("username is changed to %s" % username)
        #     dict_user["user_name"] = username
        #     fetch_users_info(browser, dict_user, session)
        # else:
        #     user_data = data["entry_data"]["ProfilePage"][0]["graphql"]["user"]
        #     dict_user["followed"] = user_data["edge_followed_by"]["count"]
        #     dict_user["follow"] = user_data["edge_follow"]["count"]
        #     dict_user["posts"] = user_data["edge_owner_to_timeline_media"]["count"]

    except Exception as ex:
        print('error@fetch_users_info', ex)
        print(source)