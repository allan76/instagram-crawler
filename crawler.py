# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import argparse
import json
import sys
from io import open

from inscrawler import InsCrawler
from inscrawler.settings import override_settings
from inscrawler.settings import prepare_override_settings

from inscrawler.db import verify_distance

import os
from subprocess import Popen
import subprocess

def usage():
    return """
        python crawler.py posts -u cal_foodie -n 100 -o ./output
        python crawler.py posts_full -u cal_foodie -n 100 -o ./output
        python crawler.py profile -u cal_foodie -o ./output
        python crawler.py profile_script -u cal_foodie -o ./output
        python crawler.py hashtag -t taiwan -o ./output
        python crawler.py verify_place -t 가로수길 -c 37.520734,127.022938 -d 10
        python3 crawler.py verify_place -t 한남동 -c 37.536828,127.005207 -d 3
        python crawler.py verify_place -t 을지로 -c 37.566777,126.994750 -d 1
        python crawler.py place -t 가로수길 -n 10 -o ./output
        python crawler.py geocode -t 을지로
        python crawler.py posts_location -o ./output
        python crawler.py posts_location_multi -i 10
        python crawler.py posts_location_multi -i 3 --debug
        python crawler.py posts_location_id -id 0 -total 4
        python crawler.py posts_location_id -id 1 -total 4
        python crawler.py posts_location_id -id 2 -total 4
        python crawler.py users_info -id 0 -total 8
        python crawler.py users_info -id 1 -total 8
        The default number for fetching posts via hashtag is 100.
    """


def get_posts_by_user(username, number, detail, debug):
    ins_crawler = InsCrawler(has_screen=debug)
    return ins_crawler.get_user_posts(username, number, detail)


def get_profile(username):
    ins_crawler = InsCrawler()
    return ins_crawler.get_user_profile(username)


def get_profile_from_script(username):
    ins_cralwer = InsCrawler()
    return ins_cralwer.get_user_profile_from_script_shared_data(username)


def get_posts_by_hashtag(tag, number, debug):
    ins_crawler = InsCrawler(has_screen=debug)
    return ins_crawler.get_latest_posts_by_tag(tag, number)

def get_places_by_hashtag(tag, number, debug):
    ins_crawler = InsCrawler(has_screen=debug)
    return ins_crawler.get_places_by_tag(tag, number)

def verify_places(tag, tag_coord, distance):
    print("start")
    verify_distance(tag, tag_coord, distance)
    print("end")

def arg_required(args, fields=[]):
    for field in fields:
        if not getattr(args, field):
            parser.print_help()
            sys.exit()

def output(data, filepath):
    out = json.dumps(data, ensure_ascii=False)
    if filepath:
        with open(filepath, "w", encoding="utf8") as f:
            f.write(out)
    else:
        print(out)

def update_geocode(tag, debug):
    ins_crawler = InsCrawler(has_screen=debug)
    ins_crawler.update_geocode(tag)

def get_posts_by_location(debug):
    ins_crawler = InsCrawler(has_screen=debug)
    return ins_crawler.get_location_posts()

def get_posts_by_location_multi(cnt, debug):
    for i in range(0,cnt):
        if debug:
            Popen('python crawler.py posts_location_id -id %s -total %s --debug' % (i, cnt), creationflags=subprocess.CREATE_NEW_CONSOLE)
        else:
            Popen('python crawler.py posts_location_id -id %s -total %s' % (i, cnt), creationflags=subprocess.CREATE_NEW_CONSOLE)

def get_posts_by_location_id(index, total, debug):
    ins_crawler = InsCrawler(has_screen=debug)
    return ins_crawler.get_location_posts_by_id(index, total)

def get_users_info(index, total, debug):
    ins_crawler = InsCrawler(has_screen=debug)
    return ins_crawler.get_users_info(index, total)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Instagram Crawler", usage=usage())
    parser.add_argument(
        "mode", help="options: [posts, posts_full, profile, profile_script, hashtag, place, verify_place, geocode, posts_location, posts_location_multi, posts_location_id, users_info]"
    )
    parser.add_argument("-n", "--number", type=int, help="number of returned posts")
    parser.add_argument("-u", "--username", help="instagram's username")
    parser.add_argument("-t", "--tag", help="instagram's tag name")
    parser.add_argument("-o", "--output", help="output file name(json format)")
    parser.add_argument("--debug", action="store_true")
    parser.add_argument("-c", "--coord", help="tag's coord")
    parser.add_argument("-d", "--distance", type=int, help="distance from tag's coord")
    parser.add_argument("-i", "--instance", type=int, help="instance count")

    parser.add_argument("-id", "--index", type=int, help="instance count")
    parser.add_argument("-total", "--total", type=int, help="instance count")

    prepare_override_settings(parser)

    args = parser.parse_args()

    override_settings(args)

    if args.mode in ["posts", "posts_full"]:
        arg_required("username")
        output(
            get_posts_by_user(
                args.username, args.number, args.mode == "posts_full", args.debug
            ),
            args.output,
        )
    elif args.mode == "profile":
        arg_required("username")
        output(get_profile(args.username), args.output)
    elif args.mode == "profile_script":
        arg_required("username")
        output(get_profile_from_script(args.username), args.output)
    elif args.mode == "hashtag":
        arg_required("tag")
        output(
            get_posts_by_hashtag(args.tag, args.number or 100, args.debug), args.output
        )
    elif args.mode == "place":
        arg_required("tag")
        output(
            get_places_by_hashtag(args.tag, args.number or 100, args.debug), args.output
        )
    elif args.mode == "verify_place":
        arg_required("tag")
        arg_required("coord")
        arg_required("distance")
        coord = (float(args.coord.split(',')[0]), float(args.coord.split(',')[1]))
        verify_places(args.tag, coord, args.distance)
    elif args.mode == "geocode":
        _tag = "" if args.tag is None else args.tag
        update_geocode(_tag, args.debug)
    elif args.mode == "posts_location":
        # output(
        #     get_posts_by_location(
        #         args.debug
        #     ),
        #     args.output,
        # )
        get_posts_by_location(args.debug)
    elif args.mode == "posts_location_multi":
        arg_required("instance")
        get_posts_by_location_multi(args.instance, args.debug)
    elif args.mode == "posts_location_id":
        arg_required("index")
        arg_required("total")
        get_posts_by_location_id(args.index, args.total, args.debug)
    elif args.mode == "users_info":
        arg_required("index")
        arg_required("total")
        get_users_info(args.index, args.total, args.debug)
    else:
        usage()
